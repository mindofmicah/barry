<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('grab information from a site');


$mercenary =  new Juicing\Mercenaries\JuiceRecipeMercenary;
$mercenary->takeCareOf('ginger-paradise-21');


$I->seeRecord('recipes', ['name'=>'Ginger Paradise', 'url'=>'https://juicerecipes.com/recipes/ginger-paradise-21']);
$r = $I->grabRecord('recipes', ['name'=>'Ginger Paradise', 'url'=>'https://juicerecipes.com/recipes/ginger-paradise-21']);
$recipe = $I->grabRecord('recipes', ['name'=>'Ginger Paradise', 'url'=>'https://juicerecipes.com/recipes/ginger-paradise-paradise-21']);

$I->seeRecord('ingredients', ['name'=>'Apple']);
$I->seeRecord('ingredients', ['name'=>'Carrots']);
$I->seeRecord('ingredients', ['name'=>'Ginger Root']);

$i1 = $I->grabRecord('ingredients', ['name'=>'Apple']);
$i2 = $I->grabRecord('ingredients', ['name'=>'Carrots']);
$i3 = $I->grabRecord('ingredients', ['name'=>'Ginger Root']);

$I->seeRecord(
    'ingredient_recipe', 
    [
        'recipe_id' => $r->id, 
        'ingredient_id'=>$i1->id, 
        'amount' => '1 medium (3"dia)'
    ]
);

$I->seeRecord('ingredient_recipe', ['recipe_id' => $r->id, 'ingredient_id'=>$i2->id, 'amount'=>'4 medium']);

$I->seeRecord('ingredient_recipe', ['recipe_id' => $r->id, 'ingredient_id'=>$i3->id, 'amount'=>'1 thumb (1" dia)']);
