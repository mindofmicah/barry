<?php 
$I = new CliTester($scenario);
$I->wantTo('create a new repository for a Wrestler');

$I->cleanDir('tests/tmp');

$I->runShellCommand('php '.base_path('artisan') . ' barry:generate Wrestler --path="tests/tmp/" --func="all" --func="create(array input)"');

$I->openFile('tests/tmp/Repositories/Wrestler/WrestlerRepositoryInterface.php');
$I->seeFileContentsEqual(file_get_contents('tests/cli/stubs/interface.stub'));

$I->openFile('tests/tmp/Repositories/Wrestler/EloquentWrestlerRepository.php');

$I->seeFileContentsEqual(file_get_contents('tests/cli/stubs/eloquent.stub'));

$I->cleanDir('tests/tmp');
