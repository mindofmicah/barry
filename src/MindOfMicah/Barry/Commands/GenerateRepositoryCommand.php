<?php namespace MindOfMicah\Barry\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Illuminate\Filesystem\Filesystem;
use Mustache_Engine;
use MindOfMicah\Barry\Parsers\FunctionNameParser;

class GenerateRepositoryCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'barry:generate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Filesystem $file, Mustache_Engine $mustache, FunctionNameParser $parser)
	{
        $this->file = $file;
        $this->mustache = $mustache;
        $this->parser = $parser;
		parent::__construct();
	}

    private function grabPath()
    {
        return $this->option('path') . '/' . $this->option('namespace') . '/' . $this->argument('model') . '/';;
    }
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $path = $this->grabPath();
        $model = $this->argument('model');
        if (!$this->file->isDirectory($this->grabPath())) {
            $this->file->makeDirectory($this->grabPath(), 0755, true);
        }
    
        $templates = [];
        $templates[] = ['template_path'=>__DIR__ . '/../../../../templates/interface.template', 'destination'=> $path . $this->buildInterfaceName($model) . '.php'];
        $templates[] = ['template_path'=>__DIR__ . '/../../../../templates/eloquent.template', 'destination'=> $path . $this->buildEloquentName($model) . '.php'];

        foreach ($templates as $template) {
            $contents = $this->mustache->render($this->file->get($template['template_path']), $this->buildTemplateData($model)); 
            $this->file->put(
                $template['destination'],
                $contents
            );
        }
        $this->line('Created Repositories');
	}

    private function buildInterfaceName()
    {
        $model = $this->argument('model');
        return "{$model}RepositoryInterface";
    }
    private function buildEloquentName()
    {
        $model = $this->argument('model');
        return "Eloquent{$model}Repository";
    }
    private function buildNamespace()
    {
        return $this->option('name') . '\\' . $this->option('namespace') . '\\' . $this->argument('model');
    }

    private function buildModelNamespace()
    {
        return $this->option('name') . '\\' . $this->option('model_namespace') . '\\' . $this->argument('model');
    }
    private function buildTemplateData()
    {
        return [
            'namespace'       => $this->buildNamespace(), 
            'class_name'      => $this->buildEloquentName(), 
            'interface_name'  => $this->buildInterfaceName(),
            'model_name'      => $this->argument('model'),
            'model_namespace' => $this->buildModelNamespace(),
            'functions'       => $this->buildFunctions()
        ];
    }

    private function buildFunctions()
    {
        $ret = [];
        foreach ($this->option('func') as $index => $function) {

            $ret[] = [
                'name'=> $this->prepFunctionName($function),
                'has_more' => true
            ];
        }
        $ret[$index]['has_more'] = false;

        return $ret;
        return [
            ['name' => 'all()', 'has_more'=>true],
            ['name' => 'create(array $input)', 'has_more'=>false],
        ];
    }
    
    private function prepFunctionName($function)
    {
        return $this->parser->parse($function); 
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['model', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['path', null, InputOption::VALUE_OPTIONAL, 'An example option.', app_path()],
			['namespace', null, InputOption::VALUE_OPTIONAL, 'An example option.', 'Repositories'],
			['name', null, InputOption::VALUE_OPTIONAL, 'An example option.', 'App'],
			['model_namespace', null, InputOption::VALUE_OPTIONAL, 'An example option.', 'Entities'],
            ['func', 'f', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '',[]],
		];
	}

}
