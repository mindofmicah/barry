<?php
namespace MindOfMicah\Barry\Parsers;

class FunctionNameParser
{
    public function parse($original)
    {
        if (!preg_match('/(.+)\((.*)\)$/', $original, $arguments)) {
            return $original . '()';
        }

        return $arguments[1] . '(' . $this->formatArgument($arguments[2]) . ')';
    }

    private function formatArgument($argument)
    {
        $typehint = '';
        if (strpos($argument, ' ') !== false) {
            list($typehint, $argument) = explode(' ', $argument, 2);
        }
        return trim($typehint . ' $' . $argument);
    }
}
