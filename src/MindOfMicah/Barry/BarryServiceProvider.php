<?php namespace MindOfMicah\Barry;


use Illuminate\Support\ServiceProvider;
use MindOfMicah\Barry\Parsers\FunctionNameParser;
class BarryServiceProvider extends ServiceProvider {

	public function boot()
	{
//        dd('provider');
	}
    public function register()
    {
        // code...
        $this->app->bindShared('commands.barry', function () {
            $file = $this->app['files'];
            return new Commands\GenerateRepositoryCommand($file, new \Mustache_Engine, new FunctionNameParser);
        });
        $this->commands('commands.barry');
    }
}
