<?php

namespace spec\MindOfMicah\Barry\Parsers;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FunctionNameParserSpec extends ObjectBehavior
{
    public function it_can_appends_parens_if_needed()
    {
        $this->parse('all')->shouldEqual('all()');
    }

    public function it_adds_dollar_signs_to_variables()
    {
        $this->parse('find(id)')->shouldEqual('find($id)');
    }

    public function it_allows_basic_typehinting()
    {
        $this->parse('create(array input)')->shouldEqual('create(array $input)');
    }
}
